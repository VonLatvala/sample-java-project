def artifactoryUrl = "https://artifactory.alatvala.fi/artifactory"
def artifactoryCredId = "jenkins-bot-artifactory-encrypted"
def artifactoryReleaseRepo = "sample-java-project-release"
def artifactorySnapshotRepo = "sample-java-project-snapshot"
def mvnResolverRepo = "mvn-remote"
def mvnTool = "Maven 3.6.3"
def sonarEnv = "VonLatvala Sonar"
def sonarScannerTool = "SonarScanner 4.0.0.1744"
def mvnGoals = "install"

def project = "sample-java-project"
def sonarProjectKey = "SAMPLE-JAVA-PROJECT"
def sonarSourceEncoding = "UTF-8"
def sonarSources = "src/"
def sonarJavaBinaries = "./target"
def sonarJavaLibraries = ""

pipeline {
    agent { label 'mvn' }
    stages {
        stage('Configure Artifactory') {
            steps {
                rtServer (
                    id: "ARTIFACTORY_SERVER",
                    url: "${artifactoryUrl}",
                    credentialsId: "${artifactoryCredId}"
                )

                rtMavenDeployer (
                    id: "MAVEN_DEPLOYER",
                    serverId: "ARTIFACTORY_SERVER",
                    releaseRepo: "${artifactoryReleaseRepo}",
                    snapshotRepo: "${artifactorySnapshotRepo}"
                )

                rtMavenResolver (
                    id: "MAVEN_RESOLVER",
                    serverId: "ARTIFACTORY_SERVER",
                    releaseRepo: "${mvnResolverRepo}",
                    snapshotRepo: "${mvnResolverRepo}"
                )
            }
        }

        stage('Maven Build & Publish') {
            steps {
                    script {
                    buildName = "${project}-${env.BRANCH_NAME}-${env.BUILD_NUMBER}"
                }

                println("Running maven (rtMavenRun) build '${buildName}' with goals '${mvnGoals}'")
                rtMavenRun (
                    tool: "${mvnTool}",
                    pom: 'pom.xml',
                    goals: "${mvnGoals}",
                    deployerId: "MAVEN_DEPLOYER",
                    resolverId: "MAVEN_RESOLVER",
                    buildName: "${buildName}"
                )

                script {
                    println("Built with buildName: '${buildName}'")
                    println("Publishing build info")
                }
                rtPublishBuildInfo(
                    serverId: "ARTIFACTORY_SERVER",
                    buildName: "${buildName}"
                )
            }
        }

        stage('Static Code Analysis') {
            steps {
                withSonarQubeEnv("${sonarEnv}") {
                    script {
                        scannerHome = tool "${sonarScannerTool}"

                        sonarCommand = "${scannerHome}/bin/sonar-scanner" +\
                            " -Dsonar.login=\"${SONAR_AUTH_TOKEN}\"" +\
                            " -Dsonar.projectKey=\"${sonarProjectKey}\"" +\
                            " -Dsonar.sourceEncoding=\"${sonarSourceEncoding}\"" +\
                            " -Dsonar.sources=\"${sonarSources}\"" +\
                            " -Dsonar.java.binaries=\"${sonarJavaBinaries}\"" +\
                            " -Dsonar.java.libraries=\"${sonarJavaLibraries}\""
                    }
                    sh "${sonarCommand}"
                }
            }
        }

        stage('Deploy SNAPSHOT to Testing') {
            environment {
                ART_CREDS = credentials("${artifactoryCredId}")
            }
            steps {
                script {
                    projectMvnVersion = sh (
                        script: 'mvn -q -Dexec.executable="echo" -Dexec.args=\'${project.version}\' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.6.0:exec',
                        returnStdout: true
                    ).trim()
                    println("Version: " + projectMvnVersion)
                    projectMvnGroupId = sh (
                        script: 'mvn -q -Dexec.executable="echo" -Dexec.args=\'${project.groupId}\' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.6.0:exec',
                        returnStdout: true
                    ).trim()
                    println("GroupID: " + projectMvnGroupId)
                    projectRPN = projectMvnGroupId.replaceAll('\\.', '/') + "/${project}"
                    artifactoryPathHead = "${artifactoryUrl}/${artifactorySnapshotRepo}/${projectRPN}/${projectMvnVersion}/"
                    snapshotVersion = sh (
                        script: "curl --fail -u\"$ART_CREDS\" ${artifactoryPathHead}/maven-metadata.xml | xmlstarlet sel -T -t -v '//metadata/versioning/snapshotVersions/snapshotVersion[1]/value'",
                        returnStdout: true
                    ).trim()
                    println("Snapshot version: " + snapshotVersion)
                    artifactUrl = artifactoryPathHead + "/${project}-${snapshotVersion}.jar"
                    println('JAR URL: ' + artifactUrl)
                }
                build job: '/debug/Customer/Pipeline/SJP SNAPSHOT Deployment Pipeline/', parameters: [
                    [
                        $class: 'MavenMetadataParameterValue',
                        artifactId: "${project}",
                        artifactUrl: "${artifactUrl}",
                        classifier: '',
                        description: '',
                        groupId: "${projectMvnGroupId}",
                        name: 'SOFTWARE',
                        packaging: 'jar',
                        version: "${projectMvnVersion}"
                    ], string(name: 'DEPLOY_ENV', value: 'testing')
                ]
            }
        }

        stage("Quality Gate") {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
    }
}
