package fi.alatvala.sjp;

import org.jetbrains.annotations.Contract;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) {
        double a, b;
        a = 23.2;
        b = 12.7;
        System.out.println("Let's do some arithmetic!");
        System.out.println(a + " + " + b + " = " + add(a, b));
        System.out.println("(" + a + " + " + b + ") + " + a + " = " + add(add(a, b), a));
        System.out.println(a + " - " + b + " = " + sub(a, b));
        System.out.println(a + " * " + b + " = " + mul(a, b));
        System.out.println(a + " / " + b + " = " + div(a, b));
        System.out.println("And that's all folks!");
    }

    @org.jetbrains.annotations.Contract(pure = true)
    public static double add(double a, double b) {
        return a + b;
    }

    @Contract(pure = true)
    public static double sub(double a, double b) {
        return a - b;
    }

    @Contract(pure = true)
    public static double mul(double a, double b) {
        return a * b;
    }

    @Contract(pure = true)
    public static double div(double a, double b) {
        return a / b;
    }
}
