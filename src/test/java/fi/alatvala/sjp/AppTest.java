package fi.alatvala.sjp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void add() {
        double a = 20.2;
        double b = 32.1;
        double expectedResult = a + b;
        assertTrue(App.add(a, b) == expectedResult);
    }

    @Test
    public void sub() {
        double a = 20.2;
        double b = 32.1;
        double expectedResult = a - b;
        assertTrue(App.sub(a, b) == expectedResult);
    }

    @Test
    public void mul() {
        double a = 20.2;
        double b = 32.1;
        double expectedResult = a * b;
        assertTrue(App.mul(a, b) == expectedResult);
    }

    @Test
    public void div() {
        double a = 20.2;
        double b = 32.1;
        double expectedResult = a / b;
        assertTrue(App.div(a, b) == expectedResult);
    }
}
